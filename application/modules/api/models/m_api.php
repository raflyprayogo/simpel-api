<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_api extends CI_Model {

    public function __construct(){
        parent::__construct();
    }

    public function get_initial_balance($id, $start_date){
        $sql        = "SELECT
                        COALESCE(
                            (
                                coalesce(
                                    (
                                        SELECT
                                            COALESCE(SUM(nominal), 0)
                                        FROM
                                            pengisian_saldo
                                        WHERE
                                            id_customer = '$id'
                                            AND date(tgl_pengisian) < date('$start_date') 
                                    ),0 ) + 
                                coalesce(
                                    (
                                        SELECT
                                            coalesce(SUM(nominal), 0)
                                        FROM
                                            pengisian_saldo_customer b
                                        WHERE
                                            id_customer = '$id'
                                            AND date(tgl_pengisian) < date('$start_date') 
                                    ), 0)
                            ) - (
                                    coalesce(
                                        (
                                            SELECT 
                                                coalesce(SUM(nominal), 0)
                                            FROM 
                                                penarikan_saldo_customer 
                                            WHERE 
                                                id_customer = '$id'
                                                AND date(tgl_penarikan) < date('$start_date')
                                        ), 0) + 
                                    coalesce(
                                        (
                                            SELECT
                                                SUM(c.b_adm) + SUM(c.jumlah) total
                                            FROM
                                                (
                                                    SELECT
                                                        a.biaya_transaksi b_adm,
                                                        SUM(b.jumlah) jumlah
                                                    FROM
                                                        penjualan a
                                                    LEFT JOIN penjualan_data b ON
                                                        b.id_penjualan = a.id_penjualan
                                                    WHERE
                                                        a.id_customer = '$id'
                                                        AND date(a.tgl_penjualan) < date('$start_date')
                                                    GROUP BY
                                                        a.id_penjualan
                                                ) c 
                                        ), 0) 
                                ), 0
                            ) saldo
                    FROM 
                        dual";
        $query      = $this->db->query($sql);
        $result     = $query->result();
        return $result;
    }

    public function get_mutation_header($id, $start_date, $end_date){
        $sql        = " (SELECT c.id_customer,
                                c.nama_customer,
                                ( COALESCE(ps.nominal, 0)
                                  + COALESCE(psc.nominal, 0) ) - ( COALESCE(pen_sc.nominal, 0)
                                                                   + COALESCE(j.total, 0) ) nominal,
                                'saldo awal'                                                jenis
                         FROM   customer c
                                LEFT JOIN (SELECT id_customer,
                                                  COALESCE (Sum(nominal), 0) nominal
                                           FROM   pengisian_saldo
                                           WHERE  Date(tgl_pengisian) < Date('$start_date')
                                                  AND id_customer = $id
                                           GROUP  BY id_customer) ps
                                       ON ps.id_customer = c.id_customer
                                LEFT JOIN (SELECT id_customer,
                                                  COALESCE (Sum(nominal), 0) nominal
                                           FROM   pengisian_saldo_customer b
                                           WHERE  Date(tgl_pengisian) < Date('$start_date')
                                                  AND id_customer = $id
                                           GROUP  BY id_customer) psc
                                       ON psc.id_customer = c.id_customer
                                LEFT JOIN (SELECT id_customer,
                                                  COALESCE (Sum(nominal), 0) nominal
                                           FROM   penarikan_saldo_customer
                                           WHERE  Date(tgl_penarikan) < Date('$start_date')
                                                  AND id_customer = $id
                                           GROUP  BY id_customer) pen_sc
                                       ON pen_sc.id_customer = c.id_customer
                                LEFT JOIN (SELECT c.id_customer,
                                                  Sum( c.b_adm ) + Sum( c.jumlah ) total
                                           FROM   (SELECT a.id_customer,
                                                          a.biaya_transaksi b_adm,
                                                          Sum(b.jumlah)     jumlah
                                                   FROM   penjualan a
                                                          LEFT JOIN penjualan_data b
                                                                 ON b.id_penjualan = a.id_penjualan
                                                   WHERE  Date (a.tgl_penjualan) < Date('$start_date')
                                                          AND a.id_customer = $id
                                                   GROUP  BY a.id_penjualan) c) j
                                       ON j.id_customer = c.id_customer
                         WHERE  c.id_customer = $id)
                        UNION ALL
                        (SELECT c.id_customer,
                                c.nama_customer,
                                ( COALESCE(ps.nominal, 0)
                                  + COALESCE(psc.nominal, 0) ) - ( COALESCE(pen_sc.nominal, 0)
                                                                   + COALESCE(j.total, 0) ) nominal,
                                'saldo akhir'                                               jenis
                         FROM   customer c
                                LEFT JOIN (SELECT id_customer,
                                                  COALESCE (Sum(nominal), 0) nominal
                                           FROM   pengisian_saldo
                                           WHERE  Date(tgl_pengisian) <= Date('$end_date')
                                                  AND id_customer = $id
                                           GROUP  BY id_customer) ps
                                       ON ps.id_customer = c.id_customer
                                LEFT JOIN (SELECT id_customer,
                                                  COALESCE (Sum(nominal), 0) nominal
                                           FROM   pengisian_saldo_customer b
                                           WHERE  Date(tgl_pengisian) <= Date('$end_date')
                                                  AND id_customer = $id
                                           GROUP  BY id_customer) psc
                                       ON psc.id_customer = c.id_customer
                                LEFT JOIN (SELECT id_customer,
                                                  COALESCE (Sum(nominal), 0) nominal
                                           FROM   penarikan_saldo_customer
                                           WHERE  Date(tgl_penarikan) <= Date('$end_date')
                                                  AND id_customer = $id
                                           GROUP  BY id_customer) pen_sc
                                       ON pen_sc.id_customer = c.id_customer
                                LEFT JOIN (SELECT c.id_customer,
                                                  Sum( c.b_adm ) + Sum( c.jumlah ) total
                                           FROM   (SELECT a.id_customer,
                                                          a.biaya_transaksi b_adm,
                                                          Sum(b.jumlah)     jumlah
                                                   FROM   penjualan a
                                                          LEFT JOIN penjualan_data b
                                                                 ON b.id_penjualan = a.id_penjualan
                                                   WHERE  Date (a.tgl_penjualan) <= Date('$end_date')
                                                          AND a.id_customer = $id
                                                   GROUP  BY a.id_penjualan) c) j
                                       ON j.id_customer = c.id_customer
                         WHERE  c.id_customer = $id)
                        UNION ALL
                        (SELECT id_customer,
                                nama_customer,
                                COALESCE (Sum(nominal), 0) nominal,
                                'total dana masuk'         jenis
                         FROM   ((SELECT a.id_customer,
                                         a.nama_customer,
                                         COALESCE (b.nominal, 0) nominal
                                  FROM   customer a
                                         LEFT JOIN pengisian_saldo b
                                                ON a.id_customer = b.id_customer
                                  WHERE  a.id_customer = $id
                                         AND Date(b.tgl_pengisian) >= '$start_date'
                                         AND Date(b.tgl_pengisian) <= '$end_date')
                                 UNION ALL
                                 (SELECT a.id_customer,
                                         a.nama_customer,
                                         COALESCE (b.nominal, 0) tot_pengisian
                                  FROM   customer a
                                         LEFT JOIN pengisian_saldo_customer b
                                                ON a.id_customer = b.id_customer
                                  WHERE  a.id_customer = $id
                                         AND Date(b.tgl_pengisian) >= '$start_date'
                                         AND Date(b.tgl_pengisian) <= '$end_date')) a)
                        UNION ALL
                        (SELECT a.id_customer,
                                a.nama_customer,
                                COALESCE (Sum(nominal), 0) nominal,
                                'total dana keluar'        jenis
                         FROM   ((SELECT a.id_customer,
                                         a.nama_customer,
                                         COALESCE (Sum(bb.jumlah), 0) nominal
                                  FROM   customer a
                                         LEFT JOIN penjualan aa
                                                ON aa.id_customer = a.id_customer
                                         LEFT JOIN penjualan_data bb
                                                ON aa.id_penjualan = bb.id_penjualan
                                  WHERE  a.id_customer = $id
                                         AND Date(aa.tgl_penjualan) >= '$start_date'
                                         AND Date(aa.tgl_penjualan) <= '$end_date'
                                  GROUP  BY aa.tsc_code)
                                 UNION ALL
                                 (SELECT a.id_customer,
                                         a.nama_customer,
                                         COALESCE (b.nominal, 0) tot_penarikan
                                  FROM   customer a
                                         LEFT JOIN penarikan_saldo_customer b
                                                ON a.id_customer = b.id_customer
                                  WHERE  a.id_customer = $id
                                         AND Date(b.tgl_penarikan) >= '$start_date'
                                         AND Date(b.tgl_penarikan) <= '$end_date')
                                 UNION ALL
                                 (SELECT a.id_customer,
                                         a.nama_customer,
                                         COALESCE (b.biaya_transaksi, 0) biaya_transaksi
                                  FROM   customer a
                                         LEFT JOIN penjualan b
                                                ON a.id_customer = b.id_customer
                                  WHERE  a.id_customer = $id
                                         AND Date(b.tgl_penjualan) >= '$start_date'
                                         AND Date(b.tgl_penjualan) <= '$end_date')) a)";
        $query      = $this->db->query($sql);
        $result     = $query->result();
        return $result;
    }

    public function get_purchase_history($nik, $offset, $limit){
        $sql        = "SELECT a.`code`,
                        a.`name`,
                        a.date,
                        SUM(a.total) total,
                        SUM(a.total_item) total_item,
                        SUM(a.admin_fee) admin_fee
                        FROM
                        (SELECT
                            `tsc_code` AS code,
                            `nama_customer` AS name,
                            `tgl_penjualan` AS date,
                            total.total,
                            total_item.total_item,
                            `biaya_transaksi` AS admin_fee 
                        FROM
                            ( `penjualan` p )
                            INNER JOIN `customer` c ON `c`.`id_customer` = `p`.`id_customer`
                            LEFT JOIN ( SELECT pd.id_penjualan, SUM( jumlah ) total FROM penjualan_data pd GROUP BY pd.id_penjualan ) total ON total.id_penjualan = p.id_penjualan
                            LEFT JOIN ( SELECT pd.id_penjualan, SUM( qty ) total_item FROM penjualan_data pd GROUP BY pd.id_penjualan ) total_item ON total_item.id_penjualan = p.id_penjualan 
                        WHERE
                            `nik_wali` = '$nik' 
                        ORDER BY
                            `tgl_penjualan` DESC 
                        ) a
                        GROUP BY a.`code`
                        ORDER BY a.`date` desc
                        LIMIT $limit OFFSET $offset";
        $query      = $this->db->query($sql);
        $result     = $query->result();
        return $result;
    }

    public function get_total_withdraw($id){
        $sql        = "SELECT COALESCE(SUM(nominal), 0) as total_withdraw from penarikan_saldo_customer pd WHERE id_customer = '$id'";
        $query      = $this->db->query($sql);
        $result     = $query->result();
        return $result;
    }

    public function get_child_trans_statistic($id){
        $sql        = "SELECT a.tanggal as date,
                        a.nama_customer as name,
                        SUM(a.total) total
                        FROM
                        (SELECT
                            DATE(p.tgl_penjualan) tanggal,
                            c.nama_customer,
                            total.total 
                        FROM
                            ( `penjualan` p )
                            LEFT JOIN ( SELECT pd.id_penjualan, SUM( jumlah ) total FROM penjualan_data pd GROUP BY pd.id_penjualan ) total 
                                ON p.id_penjualan = total.id_penjualan 
                            LEFT JOIN customer c ON c.id_customer = p.id_customer
                        WHERE
                            c.id_customer = '$id') a
                        WHERE a.tanggal BETWEEN DATE_ADD(CURRENT_DATE,INTERVAL -7 DAY) AND CURRENT_DATE
                        GROUP BY a.tanggal
                        ORDER BY a.tanggal asc";
        $query      = $this->db->query($sql);
        $result     = $query->result();
        return $result;
    }

    public function get_balance_per_child($id){
        $sql        = "SELECT
                      c.id_customer,
                      c.nama_customer name,
                      c.saldo balance,
                      l.nominal,
                      COALESCE ( p.total, 0 ) total_purchase,
                      COALESCE ( p.adm, 0 ) total_purchase_adm,
                      COALESCE ( w.total_withdraw, 0 ) total_withdraw,
                      ( COALESCE ( p1.pengisian, 0 ) + COALESCE ( p2.pengisian, 0 ) ) total_topup,
                      ( COALESCE ( e.adm, 0 ) + COALESCE ( e.total, 0 ) ) total_event 
                    FROM
                      customer c
                      LEFT JOIN (
                      SELECT
                        a.id_customer,
                        SUM(a.adm) adm,
                        SUM(a.total) total
                        FROM
                          (SELECT
                            p.id_customer,
                            SUM(DISTINCT p.biaya_transaksi) adm,
                            SUM( pd.jumlah ) total 
                          FROM
                            penjualan p
                            LEFT JOIN penjualan_data pd ON p.id_penjualan = pd.id_penjualan 
                            LEFT JOIN users u ON u.kode_user = p.kode_user 
                          WHERE
                            p.id_customer = $id 
                            AND u.status_limit <> 0
                            AND MONTH ( p.tgl_penjualan ) = MONTH (
                            sysdate()) 
                          GROUP BY
                            p.id_customer,p.id_penjualan) a
                          GROUP BY a.id_customer
                      ) p ON p.id_customer = c.id_customer
                      LEFT JOIN ( SELECT id_customer, COALESCE ( SUM( nominal ), 0 ) AS total_withdraw FROM penarikan_saldo_customer pd WHERE id_customer = $id GROUP BY id_customer ) w ON w.id_customer = c.id_customer
                      LEFT JOIN ( SELECT id_customer, SUM( nominal ) pengisian FROM pengisian_saldo WHERE id_customer = $id GROUP BY id_customer ) p1 ON p1.id_customer = c.id_customer
                      LEFT JOIN ( SELECT id_customer, SUM( nominal ) pengisian FROM pengisian_saldo_customer WHERE id_customer = $id GROUP BY id_customer ) p2 ON p2.id_customer = c.id_customer
                      LEFT JOIN (
                      SELECT
                        p.id_customer,
                        sum( pd.jumlah ) total,
                        sum( p.biaya_transaksi ) adm 
                      FROM
                        penjualan p
                        LEFT JOIN penjualan_data pd ON pd.id_penjualan = p.id_penjualan
                        LEFT JOIN users u ON u.kode_user = p.kode_user 
                      WHERE
                        u.status_limit = 0 
                        AND p.id_customer = $id 
                        AND MONTH ( p.tgl_penjualan ) = MONTH (
                        sysdate()) 
                      GROUP BY
                        p.id_customer 
                      ) e ON e.id_customer = c.id_customer
                      INNER JOIN `limit_transaksi` l ON `c`.`id_limit` = `l`.`id_limit` 
                    WHERE
                      c.id_customer = $id";
        $query      = $this->db->query($sql);
        $result     = $query->result();
        return $result;
    }

    public function get_nolimit_transac($id){
        $sql        = "SELECT
                        a.kode_user,
                        a.nama,
                        SUM(a.adm) + SUM(a.total) total_belanja
                        FROM
                        (SELECT
                          u.kode_user,
                          u.nama,
                          p.id_customer,
                          SUM( DISTINCT p.biaya_transaksi ) adm,
                          SUM( pd.jumlah ) total 
                        FROM
                          penjualan p
                          LEFT JOIN penjualan_data pd ON p.id_penjualan = pd.id_penjualan
                          LEFT JOIN users u ON u.kode_user = p.kode_user 
                        WHERE
                          p.id_customer = $id 
                          AND u.status_limit = 0 
                          AND MONTH ( p.tgl_penjualan ) = MONTH (
                          sysdate()) 
                        GROUP BY
                          p.id_customer,
                          p.id_penjualan ) a
                          GROUP BY a.kode_user";
      $query      = $this->db->query($sql);
      $result     = $query->result();
      return $result;
  }

  public function get_mutation_v2($id, $start_date, $end_date, $limit, $offset){
      $sql        = "SELECT
              * 
          FROM
              (
                  (
                      
          SELECT
              a.id_customer,
              a.no_induk,
              a.nama_customer,
              COALESCE ( sum( bb.jumlah ), 0 ) nominal,
              aa.tgl_penjualan tanggal,
              'Pembelian' keterangan,
              'kredit' dafault_perkiraan 
          FROM
              customer a
              LEFT JOIN penjualan aa ON aa.id_customer = a.id_customer
              LEFT JOIN penjualan_data bb ON aa.id_penjualan = bb.id_penjualan 
          WHERE
              a.id_customer = '$id' AND date(aa.tgl_penjualan) >= '$start_date' AND date(aa.tgl_penjualan) <= '$end_date'
          GROUP BY
              aa.tsc_code 
                  ) UNION ALL (
                      
          SELECT
              a.id_customer,
              a.no_induk,
              a.nama_customer,
              COALESCE ( b.nominal, 0 ) tot_pengisian,
              b.tgl_pengisian tanggal,
              'Top up dari yayasan',
              'debet' 
          FROM
              customer a
              LEFT JOIN pengisian_saldo b ON a.id_customer = b.id_customer 
          WHERE
              a.id_customer = '$id' AND date(b.tgl_pengisian) >= '$start_date' AND date(b.tgl_pengisian) <= '$end_date' 
                  ) UNION ALL (
                      
          SELECT
              a.id_customer,
              a.no_induk,
              a.nama_customer,
              COALESCE ( b.nominal, 0 ) tot_pengisian,
              b.tgl_pengisian tanggal,
              'Top up dari ummart',
              'debet' 
          FROM
              customer a
              LEFT JOIN pengisian_saldo_customer b ON a.id_customer = b.id_customer 
          WHERE
              a.id_customer = '$id' AND date(b.tgl_pengisian) >= '$start_date' AND date(b.tgl_pengisian) <= '$end_date' 
                  ) UNION ALL (
                      
          SELECT
              a.id_customer,
              a.no_induk,
              a.nama_customer,
              COALESCE ( b.nominal, 0 ) tot_penarikan,
              b.tgl_penarikan tanggal,
              'Penarikan Saldo',
              'kredit' 
          FROM
              customer a
              LEFT JOIN penarikan_saldo_customer b ON a.id_customer = b.id_customer 
          WHERE
              a.id_customer = '$id' AND date(b.tgl_penarikan) >= '$start_date' AND date(b.tgl_penarikan) <= '$end_date' 
                  ) UNION ALL (
                      
          SELECT
              a.id_customer,
              a.no_induk,
              a.nama_customer,
              COALESCE ( b.biaya_transaksi, 0 ) biaya_transaksi,
              b.tgl_penjualan tanggal,
              'Biaya Administrasi',
              'kredit' 
          FROM
              customer a
              LEFT JOIN penjualan b ON a.id_customer = b.id_customer 
          WHERE
              a.id_customer = '$id' AND date(b.tgl_penjualan) >= '$start_date' AND date(b.tgl_penjualan) <= '$end_date'
                  ) 
              ) a 
          ORDER BY
              a.tanggal ASC
              LIMIT $limit OFFSET $offset";
        $query      = $this->db->query($sql);
        $result     = $query->result();
        return $result;
    }
}