<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->headers          = $this->input->request_headers();
        $this->api_version      = $this->headers['Api-Version'];
        $this->load->model('m_auth');
        date_default_timezone_set('UTC');
        header('Content-Type: application/json');
    }

    public function index(){   
        echo "Daily Report Rest API";
        // echo md5_mod("12345678", "nanda@gmail.com");
    }

    function post_login(){
        if($this->api_version == '1'){
            $host           = $this->input->post('host');
            $uname          = $this->input->post('uname');
            $dbase          = $this->input->post('dbase');
            $pwd            = $this->input->post('pwd');
            $username       = $this->input->post('username');
            $password       = md5_mod($this->input->post('password'), $username);

            set_database($host, $uname, $pwd, $dbase);

            $check_data     = $this->m_global->get_data_all('t_ppl', null, ['username' => $username], 'kode_ppl user_id, username, password, nama_ppl name, alamat_ppl address, no_hp phone, id_unit unit_id');
        
            if(!empty($check_data)){
                if($check_data[0]->password == $password){
                    $check_data[0]->api_token = $this->generate_file_token($check_data[0]->user_id, $check_data[0]->username);
                    $this->saveFCMToken($check_data[0]->user_id);
                    echo response_builder(true, 200, $check_data[0]);
                }else{
                    echo response_builder(false, 412, null, 'password wrong');
                }
            }else{
                echo response_builder(false, 403, null, 'user not found');
            }
        }else{
            echo response_builder(false, 900);
        }
        
    }

    function post_login_monitor(){
        if($this->api_version == '1'){
            $host           = $this->input->post('host');
            $uname          = $this->input->post('uname');
            $dbase          = $this->input->post('dbase');
            $pwd            = $this->input->post('pwd');
            $username       = $this->input->post('username');
            $password       = md5_mod($this->input->post('password'), $username);

            set_database($host, $uname, $pwd, $dbase);

            $check_data     = $this->m_global->get_data_all('t_user_monitor', null, ['email' => $username], 'id user_id, nama name, email username, password');
        
            if(!empty($check_data)){
                if($check_data[0]->password == $password){
                    $check_data[0]->api_token = $this->generate_file_token($check_data[0]->user_id, $check_data[0]->username);
                    $this->saveFCMToken($check_data[0]->user_id);
                    echo response_builder(true, 200, $check_data[0]);
                }else{
                    echo response_builder(false, 412, null, 'password wrong');
                }
            }else{
                echo response_builder(false, 403, null, 'user not found');
            }
        }else{
            echo response_builder(false, 900);
        }
        
    }

    function generate_file_token($id,$email){
        $fcm_token              = $this->headers['Fcm-Token'];
        $model_name             = $this->headers['Model-Name'];
        $salt                   = $fcm_token.'%'.$model_name;
        $token                  = md5_mod($salt, $id.'%'.$email);
        $data['Api-Token']      = $token; 
        $data['Fcm-Token']      = $fcm_token;
        $data['User-Id']        = $id; 
        $data['Email']          = $email; 
        $data['Created']        = date('Y-m-d H:i:s'); 

        file_put_contents(URL_TOKENS.$token.'.json',json_encode($data));
        return $token;
    }

    function saveFCMToken($user_id){
        $token                  = $this->headers['Fcm-Token'];
        $model_name             = $this->headers['Model-Name'];
        $app_version            = $this->headers['App-Version'];
        $os_version             = $this->headers['Os-Version'];
        $platform               = $this->headers['Platform'];
        $salt                   = $model_name.'-'.$app_version.'-'.$os_version.'-'.$platform;
        $device_id              = md5_mod($user_id, $salt);

        $data['fcm_token']      = $token;   

        $check_token = $this->m_global->get_data_all('fcm', null, ['fcm_user_id' => $user_id,'fcm_device_id' => $device_id]);
        if(count($check_token) == 0){
            $data['fcm_user_id']        = $user_id;
            $data['fcm_platform']       = $platform;
            $data['fcm_device_id']      = $device_id;
            $data['fcm_app_version']    = $app_version;
            $data['fcm_os_version']     = $os_version;
            $data['fcm_model_name']     = $model_name;
            $data['fcm_createddate']    = date('Y-m-d H:i:s');
            $result = $this->m_global->insert('fcm', $data);
            // if($result['status']){
            //     echo "inserted";
            // }
        }else{
            $result = $this->m_global->update('fcm', $data, ['fcm_user_id' => $user_id, 'fcm_device_id' => $device_id]);
            // if($result){
            //     echo "updated";
            // }
        }
    }


    

}

/* End of file config.php */
/* banner: ./application/modules/config/controllers/config.php */