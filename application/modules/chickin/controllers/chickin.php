<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Chickin extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->headers          = $this->input->request_headers();
        $this->api_version      = $this->headers['Api-Version'];
        $this->load->model('m_chickin');
        date_default_timezone_set('UTC');
        header('Content-Type: application/json');
        set_connection($this->headers['Connection']);
        acc_token();
    }

    public function index(){   
        // echo "SIMPEL Rest API";
        // echo md5(md5(md5("12345678")));
    }

    function get_history(){
        if($this->api_version == '1'){
            $ppl_code       = $this->headers['User-Id'];
            $id_farmer      = $this->input->post('id_farmer');
            $page           = $this->input->post('page');

            // $data           = $this->m_chickin->get_chickin($ppl_code,$id_farmer);
            $limit          = 20;
            $offset         = $limit*$page;


            $where['cp.status_aktif']       = '1';
            $where['cp.kode_ppl']           = $ppl_code;
            if($id_farmer != ''){
                $where['cp.id_peternak']    = $id_farmer;
            }

            $join           = [
                                ['table' => 't_peternak p', 'on' => 'cp.id_peternak = p.id_peternak', 'tipe' => 'LEFT']
                            ];
            $select         = "cp.id_chickin_ppl,cp.id_peternak,p.nama_peternak,cp.populasi,cp.harga_doc,cp.waktu_input, (CASE WHEN cp.jenis_kandang = '1' THEN 'close' ELSE 'open' END) jenis_kandang, cp.periode, cp.tanggal_chickin";
            $data           = $this->m_global->get_data_all('t_chickin_ppl cp', $join, $where, $select, null, ['cp.waktu_input', 'DESC'], $offset, $limit);
            // echo $this->db->last_query(); exit;

            echo response_builder(true, 200, $data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function get_farmer(){
        if($this->api_version == '1'){
            $query              = $this->input->post('search');
            $status             = $this->input->post('status');

            $data               = $this->m_chickin->get_farmer($query, $status);
            echo response_builder(true, 200, $data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function insert(){
        if($this->api_version == '1'){
            $ppl_code       = $this->headers['User-Id'];
            $id_farmer      = $this->input->post('id_farmer');
            $population     = $this->input->post('population');
            $type_cage      = $this->input->post('type_cage');
            $price_doc      = $this->input->post('price_doc');
            $date           = $this->input->post('date');

            $result         = $this->m_chickin->insert_chickin($id_farmer, $population, $type_cage, $price_doc, $ppl_code, $date);
            if($result[0]->done == 'done'){
                echo response_builder(true, 201, null, 'success create data');
            }else{
                echo response_builder(false, 406, null, 'failed create data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function update_chickin(){
        if($this->api_version == '1'){
            $id             = $this->input->post('id');
            $id_farmer      = $this->input->post('id_farmer');
            $population     = $this->input->post('population');
            $type_cage      = $this->input->post('type_cage');
            $price_doc      = $this->input->post('price_doc');
            $date           = $this->input->post('date');

            $data['id_peternak']        = $id_farmer;
            $data['populasi']           = $population;
            $data['jenis_kandang']      = $type_cage;
            $data['harga_doc']          = $price_doc;
            $data['tanggal_chickin']    = $date;

            $result                 = $this->m_global->update('t_chickin_ppl', $data, ['id_chickin_ppl' => $id]);
            if($result){
                echo response_builder(true, 201, null, 'success update data');
            }else{
                echo response_builder(false, 406, null, 'failed update data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function update_status(){
        if($this->api_version == '1'){
            $id             = $this->input->post('id');
            $status         = $this->input->post('status');

            $data['status_aktif']   = $status;

            $result                 = $this->m_global->update('t_chickin_ppl', $data, ['id_chickin_ppl' => $id]);
            if($result){
                echo response_builder(true, 201, null, 'success update data');
            }else{
                echo response_builder(false, 406, null, 'failed update data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function get_dashboard_estimation(){
        if($this->api_version == '1'){
            $ppl_code       = $this->input->post('ppl_code');

            $data           = $this->m_chickin->get_dashboard_est($ppl_code);
            // echo $this->db->last_query(); exit;
            echo response_builder(true, 200, $data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function get_unit(){
        if($this->api_version == '1'){
            $data           = $this->m_global->get_data_all('t_unit');
            echo response_builder(true, 200, $data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function get_ppl_by_unit(){
        if($this->api_version == '1'){
            $unit_id        = $this->input->post('unit_id');
            $data           = $this->m_global->get_data_all('t_ppl', null, ['id_unit' => $unit_id]);
            echo response_builder(true, 200, $data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function get_banner(){
        if($this->api_version == '1'){
            $data           = $this->m_global->get_data_all('t_banner', null, ['status' => '1']);
            echo response_builder(true, 200, $data);
        }else{
            echo response_builder(false, 900);
        }
    }
}

/* End of file config.php */
/* banner: ./application/modules/config/controllers/config.php */