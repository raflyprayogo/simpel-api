<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_chickin extends CI_Model {

    public function __construct(){
        parent::__construct();
    }

    public function get_chickin($ppl_code, $farmer_id){
        $sql        = "SELECT
						cp.id_chickin_ppl,
						cp.id_peternak,
						p.nama_peternak,
						cp.populasi,
						cp.harga_doc,
						cp.waktu_input,
						cp.tanggal_chickin,
						CASE
						WHEN cp.jenis_kandang = '1' THEN
						'close'
						ELSE
						'open'
						END jenis_kandang,
						cp.periode
						FROM
						t_chickin_ppl cp
						LEFT JOIN t_peternak p ON cp.id_peternak = p.id_peternak
						WHERE cp.kode_ppl = '$ppl_code'
						AND cp.id_peternak = '$farmer_id'
						AND cp.status_aktif = '1'
						ORDER BY cp.waktu_input DESC";
        $query      = $this->db->query($sql);
        $result     = $query->result();
        return $result;
    }

    public function insert_chickin($id, $population, $type, $price_doc, $ppl_code, $date){
        $sql        = "call insert_chickin_ppl($id,$population,$type,$price_doc,'$ppl_code','$date')";
        $query      = $this->db->query($sql);
        $result     = $query->result();
        return $result;
    }

    public function get_farmer($search, $stat){
    	if($stat == '0'){
    		$where  = "p.id_peternak NOT IN (SELECT id_peternak FROM t_chickin_ppl WHERE status_aktif = '1')";
    	}else{
    		$where  = "cp.status_aktif = '1'";
    	}
		$sql        = "SELECT p.id_peternak id, p.nama_peternak `name`, cp.id_chickin_ppl
						FROM t_peternak p
						LEFT JOIN t_chickin_ppl cp ON cp.id_peternak = p.id_peternak
						WHERE $where AND
						p.nama_peternak LIKE  '%$search%'
						GROUP BY p.id_peternak
						LIMIT 10";
        $query      = $this->db->query($sql);
        $result     = $query->result();
        return $result;
    }

    public function get_dashboard_est($ppl_code){
        $sql        = "SELECT
						cp.id_chickin_ppl,
						cp.id_peternak,
						tp.nama_peternak,
						cp.populasi,
						cp.jenis_kandang,
						cp.harga_doc,
						cp.periode,
						cp.tanggal_chickin,
						cp.waktu_input waktu_input_chickin,
						r.id_record,
						rp.berat_ayam,
						rp.kematian,
						rp.pemakaian_pakan,
						rp.waktu_input waktu_input_record 
					FROM
						t_chickin_ppl cp
						LEFT JOIN ( SELECT id_chickin_ppl, MAX( id_record ) id_record FROM t_recording_ppl GROUP BY id_chickin_ppl ) r ON cp.id_chickin_ppl = r.id_chickin_ppl
						LEFT JOIN t_recording_ppl rp ON r.id_record = rp.id_record
						LEFT JOIN t_peternak tp ON tp.id_peternak = cp.id_peternak 
					WHERE
						cp.kode_ppl = '$ppl_code' 
						AND cp.status_aktif = '1'";
        $query      = $this->db->query($sql);
        $result     = $query->result();
        return $result;
    }


}