<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Recording extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->headers          = $this->input->request_headers();
        $this->api_version      = $this->headers['Api-Version'];
        $this->load->model('m_recording');
        date_default_timezone_set('UTC');
        header('Content-Type: application/json');
        set_connection($this->headers['Connection']);
        acc_token();
    }

    public function index(){   
        // echo "SIMPEL Rest API";
        // echo md5(md5(md5("12345678")));
    }

    function get_history(){
        if($this->api_version == '1'){
            $id             = $this->input->post('chickin_id');
            $startdate      = $this->input->post('startdate');
            $enddate        = $this->input->post('enddate');
            $ppl_code       = $this->input->post('ppl_code');
            // $id             = 1;

            $data           = $this->m_recording->get_recording($id, $startdate, $enddate, $ppl_code);

            // echo $this->db->last_query(); exit;

            echo response_builder(true, 200, $data);
        }else{
            echo response_builder(false, 900);
        }
    }

    function insert(){
        if($this->api_version == '1'){
            $chickin_id = $this->input->post('chickin_id');
            $feed       = $this->input->post('feed');
            $weight     = $this->input->post('weight');
            $deaths     = $this->input->post('deaths');

            $data['id_chickin_ppl']     = $chickin_id;
            $data['berat_ayam']         = $weight;
            $data['kematian']           = $deaths;
            $data['pemakaian_pakan']    = $feed;

            $result         = $this->m_global->insert('t_recording_ppl', $data);
            if($result['status']){
                echo response_builder(true, 201, null, 'success create data');
            }else{
                echo response_builder(false, 406, null, 'failed create data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

    function update_record(){
        if($this->api_version == '1'){
            $id             = $this->input->post('id');
            $feed           = $this->input->post('feed');
            $weight         = $this->input->post('weight');
            $deaths         = $this->input->post('deaths');

            $data['berat_ayam']         = $weight;
            $data['kematian']           = $deaths;
            $data['pemakaian_pakan']    = $feed;

            $result                     = $this->m_global->update('t_recording_ppl', $data, ['id_record' => $id]);
            if($result){
                echo response_builder(true, 201, null, 'success update data');
            }else{
                echo response_builder(false, 406, null, 'failed update data');
            }
        }else{
            echo response_builder(false, 900);
        }
    }

}

/* End of file config.php */
/* banner: ./application/modules/config/controllers/config.php */