<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_recording extends CI_Model {

    public function __construct(){
        parent::__construct();
    }

    public function get_recording($id, $start_date, $end_date, $ppl_code){
        $sql        = "SELECT
						cp.id_chickin_ppl,
						cp.id_peternak,
						p.nama_peternak,
						cp.populasi,
						cp.harga_doc,
						CASE
						WHEN cp.jenis_kandang = '1' THEN
						'close' ELSE 'open' 
						END jenis_kandang,
						cp.periode,
						r.id_record,
						r.berat_ayam,
						r.kematian,
						r.pemakaian_pakan 
					FROM
						t_chickin_ppl cp
						LEFT JOIN t_peternak p ON cp.id_peternak = p.id_peternak
						JOIN t_recording_ppl r ON cp.id_chickin_ppl = r.id_chickin_ppl 
					WHERE
					DATE(r.waktu_input) >= '$start_date' AND DATE(r.waktu_input) <= '$end_date'";
		if($id != ''){
			$sql 	.= " AND cp.id_chickin_ppl = $id";
		}
		if($ppl_code != ''){
			$sql 	.= " AND cp.kode_ppl = $ppl_code";
		}
        $query      = $this->db->query($sql);
        $result     = $query->result();
        return $result;
    }



}